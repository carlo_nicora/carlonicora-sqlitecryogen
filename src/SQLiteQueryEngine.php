<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen\mySqlCryogen
 * @author Carlo Nicora
 */

namespace CarloNicora\cryogen\SQLiteCryogen;

use CarloNicora\cryogen\SQL\sqlQueryEngine;
use CarloNicora\cryogen\metaField;

/**
 * Class SQLITEQueryEngine
 *
 * @package CarloNicora\cryogen\SQLITECryogen
 */
class SQLiteQueryEngine extends sqlQueryEngine{
    /**
     * Returns the type of field to be passed as type of parameters to mySql for the sql query preparation
     *
     * @param metaField $field
     * @return string
     */
    protected function getDiscriminantTypeCast(metaField $field){
        switch ($field->type){
            case 'int':
            case 'tinyint':
                $returnValue = SQLITE3_INTEGER;
                break;
            case 'decimal':
            case 'double':
            case 'float':
                $returnValue = SQLITE3_FLOAT;
                break;
            case 'text':
            case 'varchar':
                $returnValue = SQLITE3_TEXT;
                break;
            default:
                $returnValue = SQLITE3_BLOB;
                break;
        }

        return($returnValue);
    }
}