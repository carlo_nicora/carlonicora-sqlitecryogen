<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen\mySqlCryogen;
 * @author Carlo Nicora
 */

namespace CarloNicora\cryogen\SQLiteCryogen;

use CarloNicora\cryogen\structureController;
use CarloNicora\cryogen\metaTable;

/**
 * Class SQLiteStructureController
 *
 * @package CarloNicora\cryogen\SQLiteCryogen
 */
class SQLiteStructureController extends structureController{
    /**
     * @var SQLiteConnectionController $connectionController
     */
    protected $connectionController;

    /**
     * @var SQLiteCryogen cryogen
     */
    protected $cryogen;

    /**
     * Initialises the structure controller class
     *
     * @param SQLiteConnectionController $connectionController
     * @param SQLiteCryogen $cryogen
     */
    public function __construct(SQLiteConnectionController $connectionController, SQLiteCryogen $cryogen){
        $this->connectionController = $connectionController;
        $this->cryogen = $cryogen;
    }

    /**
     * Returns the structure of all the tables in the connected database
     *
     * @return array
     *
     * @todo Implement the method
     */
    public function readStructure(){
        return(null);
    }

    /**
     * Read the structure of a table from the database and returns the metaTable object
     *
     * @param $tableName
     * @return metaTable
     *
     * @todo Implement the method
     */
    public function readTableStructure($tableName){
        return(null);
    }

    /**
     * Creates a view based on the specified sql code
     *
     * @param $viewSql
     * @return bool
     */
    public function createView($viewSql){
        return($this->connectionController->connection->query($viewSql));
    }

    /**
     * Creates a table on the database using the meta table passed as parameter
     *
     * @param metaTable $metaTable
     * @param bool $isFederated
     * @param string $federatedLink
     * @return bool
     *
     * @todo Implement the method
     */
    public function createTable(metaTable $metaTable, $isFederated=false, $federatedLink=null){
        return(null);
    }

    /**
     * Updates a table on the database using the meta table passed as parameter
     *
     * @param metaTable $metaTable
     * @return bool
     *
     * @todo Implement the method
     */
    public function updateTable(metaTable $metaTable){
        $returnValue = false;

        return($returnValue);
    }
}
?>