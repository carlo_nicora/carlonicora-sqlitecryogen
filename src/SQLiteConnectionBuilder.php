<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen
 * @author Carlo Nicora
 */
namespace CarloNicora\cryogen\SQLiteCryogen;

use CarloNicora\cryogen\connectionBuilder;

/**
 * Class SQLiteConnectionBuilder
 *
 * @package CarloNicora\cryogen\SQLiteCryogen
 */
class SQLiteConnectionBuilder extends connectionBuilder{
    /**
     * @var string
     */
    public $fileName;

    /**
     * Initialises the connection parameters in the database-type-specific connection builder
     *
     * @param array $connectionValues
     */
    public function initialise($connectionValues){
        $this->databaseType = 'SQLite';

        $this->fileName = $connectionValues['databasename'];
    }

    /**
     * Extends the database name of the connection builder
     *
     * @param string $databaseName
     */
    public function extendDatabaseName($databaseName){
        $partialName = substr($this->fileName, strrchr($this->fileName,'.'));
        $partialExtension = substr($this->fileName, 0, strrchr($this->fileName,'.'));

        $this->fileName = $partialName.$databaseName.$partialExtension;
    }
}