<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen\mySqlCryogen
 * @author Carlo Nicora
 */
namespace CarloNicora\cryogen\SQLiteCryogen;

use CarloNicora\cryogen\cryogenException;
use CarloNicora\cryogen\entity;
use CarloNicora\cryogen\entityList;
use CarloNicora\cryogen\SQL\sqlCryogen;
use CarloNicora\cryogen\queryEngine;
use CarloNicora\cryogen\metaTable;
use SQLite3Stmt;
use SQLite3Result;
use Exception;

/**
 * Class SQLITECryogen
 *
 * @package CarloNicora\cryogen\SQLITECryogen
 */
class SQLITECryogen extends sqlCryogen{
    /**
     * Initialises cryogen for SQLite
     *
     * @param SQLiteConnectionBuilder $connection
     */
    public function __construct($connection){
        $returnValue = false;

        $this->connectionController = new SQLITEConnectionController();

        if ($this->connectionController->initialize($connection)){
            $this->structureController = new SQLiteStructureController($this->connectionController, $this);
            $returnValue = true;
        }

        return($returnValue);
    }

    /**
     * @param metaTable|null $meta
     * @param entity|null $entity
     * @param null $valueOfKeyField
     * @return mixed
     */
    public function generateQueryEngine(metaTable $meta=null, entity $entity=null, $valueOfKeyField=null){
        $returnValue = new SQLiteQueryEngine($meta, $entity, $valueOfKeyField);

        return($returnValue);
    }

    /**
     * Clears the resources
     */
    public function __destruct(){
        if (isset($this->connectionController) && $this->connectionController->isConnected()){
            $this->connectionController->disconnect();
        } else if (isset($this->connectionController)){
            unset($this->connectionController);
        }
    }

    /**
     * Runs the transactional INSERT, UPDATE or DELETE query on the database
     *
     * @param string $sqlStatement
     * @param array $sqlParameters
     * @param bool $isDelete
     * @param bool $generatedId
     * @return entityList
     * @throws cryogenException
     */
    protected function setActionTransaction($sqlStatement, $sqlParameters, $isDelete=false, &$generatedId = false){
        /**
         * @var SQLite3Stmt $statement
         */
        $returnValue = false;

        if ($statement = $this->connectionController->connection->prepare($sqlStatement)) {
            $this->bindParameters($statement, $sqlParameters);

            try {
                $statement->execute();
                $returnValue =  $this->connectionController->connection->changes() > 0;
            } catch (Exception $systemException){
                $exception = new cryogenException(cryogenException::ERROR_RUNNING_UPDATE_QUERY, 'error: '.$this->connectionController->connection->error);
                $exception->log();
            }

            if ($returnValue){
                if ($generatedId){
                    $generatedId = $this->connectionController->connection->lastInsertRowID();
                }
            } else {
                if (!$isDelete) {
                    if (isset($this->connectionController->connection->error) && $this->connectionController->connection->error != '') {
                        $exception = new cryogenException(cryogenException::ERROR_RUNNING_UPDATE_QUERY, 'error: ' . $this->connectionController->connection->error.'running sql:'.$sqlStatement.' with '.json_encode($sqlParameters));
                        $exception->log();
                    } else {
                        $exception = new cryogenException(cryogenException::ERROR_RUNNING_UPDATE_QUERY, 'Generic Error running: '.$sqlStatement.' with '.json_encode($sqlParameters));
                        $exception->log();
                    }
                }
            }

            $statement->close();
        } else {
            throw new cryogenException(cryogenException::ERROR_PREPARING_SQL_STATEMENT, 'error: '.$this->connectionController->connection->error.'-running sql:'.$sqlStatement.'-with parameters:'.json_encode($sqlParameters));
        }

        return($returnValue);
    }

    /**
     * Commit the INSERT, UPDATE or DELETE transaction on the database
     *
     * @param bool $commit
     * @return bool
     */
    protected function completeActionTransaction($commit){
        /**
         * @var bool $returnValue
         */
        $returnValue = true;

        return($returnValue);
    }

    /**
     * Runs the transactional SELECT query on the database
     *
     * @param queryEngine $engine
     * @param string $sqlStatement
     * @param array $sqlParameters
     * @return entityList
     * @throws cryogenException
     */
    protected function setReadTransaction(queryEngine $engine, $sqlStatement, $sqlParameters){
        /**
         * @var SQLite3Stmt $statement
         * @var entity $record
         */
        $returnValue = new entityList();

        if ($statement = $this->connectionController->connection->prepare($sqlStatement)) {
            $this->bindParameters($statement, $sqlParameters);

            $dbResult = $statement->execute();

            $selectedFields = $engine->getFieldsVariables();
            $selectedDynamicFields = $engine->getDynamicFieldsVariables();
            $selectedFields = array_merge($selectedFields, $selectedDynamicFields);

            while ($row = $dbResult->fetchArray()) {

                $record = new $engine->meta->object;

                $record->entityRetrieved = TRUE;

                foreach ($selectedFields as $selectedFieldName) {
                    $record->$selectedFieldName = $row[$selectedFieldName];
                }

                $record->setInitialValues();
                $record->setRetrieved();

                $returnValue[] = $record;
                unset($record);

                if (!isset($returnValue->meta)) {
                    $returnValue->meta = $engine->meta;
                }
            }

            $statement->close();
            unset($statement);

        } else {
            throw new cryogenException(cryogenException::ERROR_PREPARING_SQL_STATEMENT, 'error: '.$this->connectionController->connection->error.'-running sql:'.$sqlStatement.'-with parameters:'.json_encode($sqlParameters));
        }

        return($returnValue);
    }

    /**
     * Specialised transaction that counts the records matching a specific query engine on the database
     *
     * @param queryEngine $engine
     * @param string $sqlStatement
     * @param array $sqlParameters
     * @return int
     * @throws cryogenException
     */
    protected function setCountTransaction(queryEngine $engine, $sqlStatement, $sqlParameters){
        /**
         *
         * @var SQLite3Stmt $statement
         * @var SQLite3Result $dbResult
         */
        $returnValue = 0;

        if ($statement = $this->connectionController->connection->prepare($sqlStatement)) {
            $this->bindParameters($statement, $sqlParameters);

            $dbResult = $statement->execute();

            while ($row = $dbResult->fetchArray()) {
                $returnValue++;
            }

            $statement->close();
            unset($statement);
        } else {
            throw new cryogenException(cryogenException::ERROR_PREPARING_SQL_STATEMENT, 'error: '.$this->connectionController->connection->error.'-running sql:'.$sqlStatement.'-with parameters:'.json_encode($sqlParameters));
        }

        return($returnValue);
    }

    /**
     * Bind the parameters to the mysqli_stmt before preparing the SQL
     *
     * @param SQLite3Stmt $statement
     * @param array $parameters
     */
    private function bindParameters(&$statement, $parameters) {
        if (isset($parameters) && sizeof($parameters) > 1) {

            $counter = 0;
            foreach ($parameters[1] as $value) {
                $type = intval(substr($parameters[0], $counter, 1));
                $statement->bindParam($counter+1, $value, $type);
            }
        }
    }
}
?>