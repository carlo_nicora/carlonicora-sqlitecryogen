<?php
/**
 * Copyright 2015 Carlo Nicora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @license Apache
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @package CarloNicora\cryogen\mySqlCryogen
 * @author Carlo Nicora
 */

namespace CarloNicora\cryogen\SQLiteCryogen;

use CarloNicora\cryogen\connectionController;
use CarloNicora\cryogen\connectionBuilder;
use SQLite3;

/**
 * Class SQLITEConnectionController
 *
 * @package CarloNicora\cryogen\SQLiteCryogen
 */
class SQLITEConnectionController extends connectionController{
    /**
     * @var SQLiteConnectionBuilder $connectionValues
     */
    public $connectionValues;

    /**
     * Opens a connection to the database
     *
     * @return bool
     */
    public function connect(){
        $returnValue = true;

        $this->connection = new SQLite3($this->connectionValues->fileName);

        return($returnValue);
    }

    /**
     * Closes a connection to the database
     *
     * @return bool
     */
    public function disconnect(){
        if($this->isConnected()){
            $this->connection->close();
        }
    }

    /**
     * Returns the name of the database specified in the connection
     *
     * @return string
     */
    public function getDatabaseName(){
        return(null);
    }

    /**
     * Create a new Database
     *
     * @param string $databaseName
     * @return bool
     */
    public function createDatabase($databaseName){
        $returnValue = false;

        if($this->isConnected()){
            $this->disconnect();
        }
        $this->initialize(connectionBuilder::bootstrap(['type'=>'SQLite', 'databasename'=>$databaseName]));

        return($returnValue);
    }

    /**
     * Identifies if there is an active connection to the database
     *
     * @return bool
     */
    public function isConnected(){
        return(isset($this->connection));
    }
}
?>